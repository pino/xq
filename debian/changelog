xq (1.3.0-1) unstable; urgency=medium

  * New upstream release.
  * Update the build dependencies according to the upstream build system:
    - bump golang-github-antchfx-xmlquery-dev to 1.4.3
    - bump golang-github-antchfx-xpath-dev to 1.3.3
    - keep golang-github-fatih-color-dev at 1.17.0 (i.e. what's in Debian
      unstable currently, upstream requires 1.18.0)
    - bump golang-github-puerkitobio-goquery-dev to 1.10.0
    - keep golang-github-stretchr-testify-dev at 1.9.0 (i.e. what's in Debian
      unstable currently, upstream requires 1.10.0)
    - keep golang-golang-x-net-dev at 0.27.0 (i.e. what's in Debian
      unstable currently, upstream requires 0.33.0)
    - keep golang-golang-x-text-dev at 0.16.0 (i.e. what's in Debian
      unstable currently, upstream requires 0.21.0

 -- Pino Toscano <pino@debian.org>  Sun, 29 Dec 2024 23:46:57 +0100

xq (1.2.5-1) unstable; urgency=medium

  * New upstream release. (Closes: #1062216)
  * Update the build dependencies according to the upstream build system:
    - add golang-github-puerkitobio-goquery-dev
    - bump golang-any to >= 1.23
    - bump golang-github-antchfx-xmlquery-dev to 1.4.1
    - bump golang-github-antchfx-xpath-dev to 1.3.1
    - bump golang-github-fatih-color-dev to 1.17.0
    - bump golang-github-spf13-cobra-dev to 1.8.1
    - bump golang-github-spf13-pflag-dev to 1.0.5
    - bump golang-github-stretchr-testify-dev to 1.9.0
    - bump golang-golang-x-net-dev to 0.27.0 (i.e. what's in Debian
      unstable currently, upstream requires 0.28.0)
    - bump golang-golang-x-text-dev 0.16.0 (i.e. what's in Debian
      unstable currently, upstream requires 0.17.0)
    - drop golang-github-spf13-viper-dev, no more needed
  * Add "version" to DH_GOLANG_INSTALL_EXTRA, as that file is embedded during
    the build.
  * Bump Standards-Version to 4.7.0, no changes required.
  * Update copyright.

 -- Pino Toscano <pino@debian.org>  Mon, 21 Oct 2024 12:36:12 +0200

xq (1.0.0-2) unstable; urgency=medium

  * Mention also the HTML support in the description.
  * Ship the upstream man page.
  * Disable the installation of the sources, as only the binary should be
    shipped: (Closes: #1023984)
    - pass --no-source to the golang dh buildsystem on install
    - drop the "Testsuite: autopkgtest-pkg-go" field, as there is nothing in
      the binary package to be tested "as installed"
    - thanks to Shengjing Zhu <zhsj@debian.org> for the notice

 -- Pino Toscano <pino@debian.org>  Tue, 15 Nov 2022 06:24:50 +0100

xq (1.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Pino Toscano <pino@debian.org>  Fri, 11 Nov 2022 22:24:56 +0100

xq (0.0.8-1) unstable; urgency=medium

  * New upstream release.

 -- Pino Toscano <pino@debian.org>  Sat, 10 Sep 2022 18:27:27 +0200

xq (0.0.7-1) unstable; urgency=medium

  * Initial packaging.

 -- Pino Toscano <pino@debian.org>  Tue, 21 Jun 2022 03:44:05 +0200
